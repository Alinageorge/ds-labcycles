#include "BinTreeLL.cpp"

int main() {
	
    BinaryTree tree;

    // Insert some values into the tree
    tree.insert(50);
    tree.insert(30);
    tree.insert(70);
    tree.insert(20);
    tree.insert(40);
    tree.insert(60);
    tree.insert(80);

    // Perform traversals and print the values
    cout << "In-order Traversal: ";
    tree.inorder();

    cout << "Pre-order Traversal: ";
    tree.preorder();

    cout << "Post-order Traversal: ";
    tree.postorder();

    return 0;
}

