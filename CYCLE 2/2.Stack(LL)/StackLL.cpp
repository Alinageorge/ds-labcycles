#include"Stack_LL.h"

template<class T>	
Node<T>::Node(T data){
	this->data=data;
	this->link=NULL;
}

template<class T>
T Node<T>:: getData(){
	return data;
}

template<class T>
Node<T>* Node<T>:: getLink(){
	return link;
}

template<class T>
void Node<T>::setLink(Node<T>* link){
	this->link=link;
}

template<class T>
void Node<T>::setData(T data){
	this->data=data;
}

template<class T>
LinkedStack<T>::LinkedStack(){
	head=NULL;
}

template<class T>
void LinkedStack<T>::Push(T key){
	Node<T>* node1=new Node<T>(key);
	node1->setLink(head);
	head=node1;
}
template<class T>
void LinkedStack<T>::Pop(){
	if(head==NULL){
		cout<<"Stack Empty"<<endl;
	}
	else{
		Node<T>* p=head;
		Node<T>* q=p->getLink();
		while(q->getLink()!=NULL){
			p=q;
			q=q->getLink();
		}
		p->setLink(NULL);
		delete q;
	}
}
template<class T>
T LinkedStack<T>::Peek(){
	if(head==NULL){
		cout<<"Stack Empty"<<endl;
		return NULL;
	}
	else{
		Node<T>* p=head;
		while(p->getLink()!=NULL){
			p=p->getLink();
		}
		return p->getData();
	}
}
template<class T>
void LinkedStack<T>::display(){
	Node<T>* p=head;
	while (p!=NULL){
		cout<<p->getData()<<" ";
		p=p->getLink();
	}
	cout<<endl;
}
