#include<iostream>
using namespace std;

template <class T>
class Node{
		T data;
		Node<T>* link;
	public:
		Node(T);
		void setData(T);
		void setLink(Node<T>*);
		T getData();
		Node<T>* getLink();
};

template<class T>		
class LinkedStack{
		Node<T>* head;
	public:
		LinkedStack();
		void display();
		void Push(T);
		void Pop();
		T Peek();
		
};

