#include"PostFixEval.h"

Stack::Stack(){
	top=-1;
	size=100;
}

void Stack::display(){
	for(int i=top;i>=0;i--){
		cout<<S[i]<<" ";
	}
	cout<<endl;
}
				
bool Stack::isEmpty(){
	if(top==-1){
		return true;
	}
	return false;
}

bool Stack::isFull(){
	if(top==size){
		return true;
	}
	return false;
}
void Stack::Push(float x){	
	if(isFull()!=true){
		top++;
		S[top]=x;
	}
	else{
		cout<<"Stack Overflow"<<endl;
	}
}

void Stack::Pop(){
	if(isEmpty()!=true){
		top--;
	}
	else{
		cout<<"Stack Underflow"<<endl;
	}
}

float Stack::Peek(){
	if(isEmpty()!=true){
		return S[top];
	}
	else{
		return -1;
	}
}
float Stack::PostFix_Eval(const string& exp){
	Stack s;
	for(int i=0;i<exp.length();i++){
		if(exp[i]=='*'){
			float val1=s.Peek();
			s.Pop();
			float val2=s.Peek();
			s.Pop();
			s.Push(val1*val2);
		}
		else if(exp[i]=='/'){
			float val1=s.Peek();
			Pop();
			float val2=s.Peek();
			s.Pop();
			s.Push(val2/val1);
		}
		else if(exp[i]=='+'){
			float val1=s.Peek();
			s.Pop();
			float val2=s.Peek();
			s.Pop();
			s.Push(val1+val2);
		}
		else if(exp[i]=='-'){
			float val1=s.Peek();
			s.Pop();
			float val2=s.Peek();
			s.Pop();
			s.Push(val2-val1);
		}
		else{
			float op=exp[i]-'0';
			s.Push(op);
		}
	}
	return s.Peek();
}
