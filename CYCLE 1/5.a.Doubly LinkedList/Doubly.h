#include<iostream>
using namespace std;

template<class T>
class Node{
		T data;
		Node<T>* llink;
		Node<T>* rlink;
	public:
		Node(T);
		void setData(T);
		void setllink(Node<T>*);
		void setrlink(Node<T>*);
		T getData();
		Node<T>* getllink();
		Node<T>* getrlink();
};
template<class T>
class Doubly{
		Node<T>* head;
		Node<T>* tail;
	public:
		Doubly();
		void create();
		void insert_at_end(T);
		void insert_at_beg(T);
		void insert_at_pos(int,T);
		void delete_at_end();
		void delete_at_beg();
		void delete_at_pos(int);
		void display();
};
