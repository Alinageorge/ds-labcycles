#include"Doubly.h"
template<class T>
Node<T>::Node(T data){
	this->data=data;
	this->rlink=NULL;
	this->llink=NULL;		
}
template<class T>
void Node<T>::setllink(Node<T>* ll){
	llink=ll;
}
template<class T>
void Node<T>::setrlink(Node<T>* rl){
	rlink=rl;
}
template<class T>
void Node<T>::setData(T data){
	data=data;
}
template<class T>
Node<T>* Node<T>::getllink(){
	return llink;
}
template<class T>
T Node<T>::getData(){
	return data;
}
template<class T>
Node<T>* Node<T>::getrlink(){
	return rlink;
}
template<class T>
 Doubly<T>::Doubly(){
	head=NULL;
	tail=NULL;
}
/*template<class T>
void Doubly<T>::create(){
	T x;
	char option;
	Node<T>* current;
	current=NULL;
	do{
		cout<<"Enter the data:";
		cin>>x;
		Node<T>* node=new Node<T>(x);
		if(head==NULL){
			head=node;
			current=head;
		}
		else{
			current->setrlink(node);
			node->setllink(current);
			current=node;
			
		}
		cout<<"Do you want to enter numbers(y/n):";
		cin>>option;
	}while(option=='y');
}*/
template<class T>
void Doubly<T>::insert_at_end(T data){
	Node<T>* node=new Node<T>(data);
	if(head==NULL){
		head=node;
		tail=node;
	}
	else{
		node->setllink(tail);
		tail->setrlink(node);
		tail=node;
		tail->setrlink(NULL);
	}
}
template <class T>
void Doubly<T>::insert_at_beg(T key){
	Node<T>* node=new Node<T> (key);
	if (head==NULL){
		head=node;
		tail=node;
		head->setllink(NULL);
		head->setrlink(NULL);
	}
	else{
		node->setrlink(head);
		head->setllink(node);
		head=node;
		head->setllink(NULL);
	}
}
template<class T>
void Doubly<T>::insert_at_pos(int pos,T key){
	int count=1;
	if (pos==1){
		insert_at_beg(key);
	}
	else{
		Node<T>* p=head;
		while (p!=NULL and count<pos-1){
			p=p->getrlink();
			count++;
		}
		Node<T>* node=new Node<T>(key);
		node->setrlink(p->getrlink());
		node->setllink(p);
		p->setrlink(node);
	}
}
template <class T>
void Doubly<T>::delete_at_beg(){
	if (head==NULL){
		cout<<"List Empty"<<endl;
	}
	else{
		Node<T>* p=head;
		head=head->getrlink();
		head->setllink(NULL);
		delete p;
	}	  
}
template <class T>
void Doubly<T>::delete_at_end(){
		if (head==NULL){
		cout<<"List Empty"<<endl;
	}
	else{
		Node<T>* p=head;
		Node<T>* q=p->getrlink();
		while(q->getrlink()!=NULL){
			p=q;
			q=q->getrlink();
		}
		p->setrlink(NULL);
		delete(q);
	}
}
template <class T>
void Doubly<T>::delete_at_pos(int pos){
		int count=1;
		Node<T>* p=head;
		Node<T>*q=p->getrlink();
		while(q->getrlink()!=NULL and count<pos-1){
			p=q;
			q=q->getrlink();
			count++;
		}
		p->setrlink(q->getllink());
		q->getrlink()->setllink(p);
		delete(q);
}
template <class T>
void Doubly<T>::display(){
	Node<T>* p=head;
	while (p!=NULL){
		cout<<p->getData()<<" ";
		p=p->getrlink();
	}
	cout<<endl;
}
