#include "Circular.h"
template <class T>
Node<T>::Node(T data){
	this->data=data;
	this->link=NULL;
}
template <class T>
T Node<T>::getData(){
	return data;
}
template <class T>
void Node<T>::setLink(Node* link){
	this->link=link;
}
template <class T>
Node<T>* Node<T>::getLink(){
	return link;
}
template <class T>
LinkedList<T>::LinkedList(){
	head=NULL;
}
template <class T>
void LinkedList<T>::insert_at_beginning(T key){
	if (head==NULL){
		Node<T>* node=new Node<T> (key);
		node->setLink(node);
		head=node;
	}
	else{
		Node<T>* node=new Node<T> (key);
		node->setLink(head);
		Node<T>* p=head;
		while (p->getLink()!=head){
			p=p->getLink();
		}
		head=node;
		p->setLink(head);	
	}
}
template <class T>
void LinkedList<T>::insert_at_end(T key){
	if (head==NULL){
		Node<T>* node=new Node<T> (key);
		node->setLink(node);
		head=node;
	}
	else{
		Node<T>* p=head;
		while(p->getLink()!=head){
			p=p->getLink();
		}
		Node<T>* node=new Node<T>(key);
		node->setLink(head);
	    p->setLink(node);
	}	
}
template <class T>
void LinkedList<T>::insert_at_pos(int pos,T key){
	if (pos==1){
		insert_at_beginning(key);
	}
	else{
		int count=1;
		Node<T>* p=head;
		while (p->getLink()!=head and count<pos-1){
			p=p->getLink();
			count++;
		}
		Node<T>* q=p->getLink();
		Node<T>* node=new Node<T>(key);
		node->setLink(q);
		p->setLink(node);
	}
}
template <class T>
void LinkedList<T>::delete_from_front(){
	if (head==NULL){
		cout<<"List Empty"<<endl;
	}
	else{
		Node<T>* p=head;
		head=head->getLink();
		Node<T>* q=head;
		do{
			q=q->getLink();
		}while(q->getLink()!=p);
		q->setLink(head);
		delete p;	  
	}
}
template <class T>
void LinkedList<T>::delete_from_end(){
	if (head==NULL){
		cout<<"List Empty"<<endl;
	}
	else{
		Node<T>* p=head;
		Node<T>* q=p->getLink();
		while(q->getLink()!=head){
			p=q;
			q=q->getLink();
		}
		p->setLink(head);
		delete(q);
	}
}
template <class T>
void LinkedList<T>::delete_from_pos(int pos){
	if (head==NULL){
		cout<<"List Empty"<<endl;
	}

	int count=1;
	Node<T>* p=head;
	Node<T>*q=p->getLink();
	while(q->getLink()!=head and count<pos-1){
		p=q;
		q=q->getLink();
		count++;
	}

	p->setLink(q->getLink());
	delete(q);

}
template <class T>
void LinkedList<T>::display(){
	Node<T>* p=head;
	do{
		cout<<p->getData()<<" ";
		p=p->getLink();
	}while(p!=head);
	cout<<endl;
}
