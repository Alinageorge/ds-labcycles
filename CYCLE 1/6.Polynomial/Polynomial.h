#include<iostream>
using namespace std;

class Node{
		int coef,exp;
		Node* link;
	public:
		Node(int,int);
		void setCoef(int);
		void setExp(int);
		void setLink(Node*);
		int getCoef();
		int getExp();
		Node* getLink();
};
		
class Polynomial{
		Node* head;
	public:
		Polynomial();
		void setHead(Node*);
		void createLL();
		void display();
		void LL_insert_at_End(int,int);
		Polynomial addPoly(Polynomial&);
		Polynomial Multiply(Polynomial&);
};
