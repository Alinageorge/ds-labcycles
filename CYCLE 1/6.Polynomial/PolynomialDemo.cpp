#include"Polynomial.cpp"

int main(){
	Polynomial ll1;
	ll1.createLL();
	cout<<"Polynomial1:";
	ll1.display();
	Polynomial ll2;
	ll2.createLL();
	cout<<"Polynomial2:";
	ll2.display();
	Polynomial sum=ll1.addPoly(ll2);
	cout<<"Sum:";
	sum.display();
    Polynomial prod=ll1.Multiply(ll2);
	cout<<"Product:";
	prod.display();
	return 0;	
}
