#include"Polynomial.h"
	
Node::Node(int c,int e){
	this->coef=c;
	this->exp=e;
	this->link=NULL;
}
int Node:: getCoef(){
	return coef;
}
int Node:: getExp(){
	return exp;
}
Node* Node:: getLink(){
	return link;
}
void Node::setLink(Node* link){
	this->link=link;
}
void Node::setCoef(int c){
	this->coef=c;
}
void Node::setExp(int e){
	this->exp=e;
}


Polynomial::Polynomial(){
	head=NULL;
}
void Polynomial::setHead(Node* h){
	head=h;
}
void Polynomial::createLL(){
	int x,n;
	char option;
	Node* current;
	current=head;
	do{
		cout<<"Enter the coefficient:";
		cin>>x;
		cout<<"Enter the exponent:";
		cin>>n;
		Node* node=new Node(x,n);
		if(head==NULL){
			head=node;
			current=head;
		}
		else{
			current->setLink(node);
			current=node;
		}
		cout<<"Do you want to enter numbers(y/n):";
		cin>>option;
	}while(option=='y');
}
void Polynomial::display(){
	Node* p=head;
	while (p!=NULL){
		cout<<p->getCoef()<<" "<<p->getExp()<<"  ";
		p=p->getLink();
	}
	cout<<endl;
}
void Polynomial::LL_insert_at_End(int c,int e){
	if (head==NULL){
		Node* node=new Node(c,e);
		head=node;
	}
	else{
		Node* p=head;
		while (p->getLink() !=NULL){
			p=p->getLink();
		}
		Node* node2=new Node(c,e);
		p->setLink(node2);	
	}
}
/*Node* Polynomial::addPoly(Polynomial& poly){
	Node* p=head;
	Node* q=poly.head;
	Node* r=new Node(NULL,NULL);	
	Node* current=r;
	while(p!=NULL && q!=NULL){
		if(p->getExp()>q->getExp()){
			int n=p->getExp();
			int x=p->getCoef();
			Node* node=new Node(x,n);
			if(current == NULL){
				current = node;
			}else{
			current->setLink(node);
			current=node;
			p=p->getLink();
		}
		}
		else if(p->getExp()<q->getExp()){
			int n=q->getExp();
			int x=q->getCoef();
			Node* node=new Node(x,n);
			current->setLink(node);
			current=node;
			q=q->getLink();
		}
		else{
			int n=p->getExp();
			int x=p->getCoef()+q->getCoef();
			Node* node=new Node(x,n);
			current->setLink(node);
			current=node;
			p=p->getLink();
			q=q->getLink();
		}
	}

	return r;
}
Node* Polynomial::Multiply(Polynomial& poly){
	Node* p=head->getLink();
	Node* q=poly.head->getLink();
	while(p!=NULL){
		Node* node=p->getLink();
		q=q->getLink();
	}
	while(q!=NULL){
		int n=p->getExp()+q->getExp();
		int x=p->getCoef()+q->getCoef();
		Node* node=new Node(x,n);
	}
	return node;
}*/
Polynomial Polynomial::addPoly(Polynomial& poly){
	Node* p=head;
	Node* q=poly.head;
	Polynomial result;
	while (p!=NULL and q!=NULL){
		if (p->getExp()==q->getExp()){
			result.LL_insert_at_End(p->getCoef()+q->getCoef(),p->getExp());
			p=p->getLink();
			q=q->getLink();
		}
		else if(p->getExp()>q->getExp()){
			result.LL_insert_at_End(p->getCoef(),p->getExp());
			p=p->getLink();
		}
		else{
			result.LL_insert_at_End(q->getCoef(),q->getExp());
			q=q->getLink();
		}
	}
	return result;
}
Polynomial Polynomial::Multiply(Polynomial& poly){
	Node*p=head;
	Polynomial result;
	while(p!=NULL){
		Node*q=poly.head;
		while(q!=NULL){
			result.LL_insert_at_End(p->getCoef()*q->getCoef(),p->getExp()+q->getExp());
			q=q->getLink();
		}
		p=p->getLink();
	}
	return result;
}
