#include"Playlist.cpp"

int main(){
	Playlist play;
	play.createPL();
	play.displayPL();
	cout<<"Playlist sorted according to Title"<<endl;
	play.sortTitle();
	play.displayPL();
	cout<<"Playlist sorted according to Artist"<<endl;
	play.sortArtist();
	play.displayPL();
	cout<<"Playlist sorted according to Duration"<<endl;
	play.sortDuration();
	play.displayPL();
	cout<<"Playlist sorted according to Genre"<<endl;
	play.sortGenre();
	play.displayPL();
}
