#include"LinkedList.cpp"
using namespace std;
int main(){
	char stat;
	cout<<"Do you want to create a Linked List(y/n):";
	cin>>stat;
	while(stat=='y'){
		cout<<"Enter the datatype of LinkedList to be created?";
		string op;
		cin>>op;
		if(op=="int"){
			int el,ch,n,pos;
			char choice;
			LinkedList<int> ll;
			ll.createLL();
			cout<<"Created LinkedList:";
			ll.display();
			cout<<"Start operations(y/n)?:";
			cin>>choice;
			while(choice=='y'){
				cout<<"Choose the actions to perform:"<<endl;
				cout<<"1.Insert at End \n2.Insert at Beginning \n3.Inserting at a position:"<<endl;
				cout<<"4.Delete at End \n5.Delete at Beginning \n6.Delete at specified position"<<endl;
				cin>>ch;
				if(ch==1){
					cout<<"How many elements do you want to insert at end?";
					cin>>n;
					for(int i=0;i<n;i++){
						cout<<"Enter the element:";
						cin>>el;			
						ll.LL_insert_at_End(el);
						cout<<"Inserting at the end:"<<endl;
						ll.display();
					}
				}
				if(ch==2){
					cout<<"How many elements do you want to insert at beginning?";
					cin>>n;
					for(int i=0;i<n;i++){
						cout<<"Enter the element:";
						cin>>el;			
						ll.LL_insert_at_Beg(el);
						cout<<"Inserting at the beginnning:"<<endl;
						ll.display();
					}
				}
				if(ch==3){					
					cout<<"Enter the element:";
					cin>>el;
					cout<<"Enter the position to insert";
					cin>>pos;			
					ll.LL_insert_at_Pos(el,pos);
					cout<<"Inserting at "<<pos<<"th position:"<<endl;
					ll.display();
				}
				if(ch==4){			
						ll.LL_Del_at_End();
						cout<<"Deleting at the end:"<<endl;
						ll.display();				
				}
				if(ch==5){			
						ll.LL_Del_at_Beg();
						cout<<"Delete at the Beginning:"<<endl;
						ll.display();
				}
				if(ch==6){
					cout<<"Enter the key to delete";
					cin>>pos;			
					ll.LL_Del_a_key(pos);
					cout<<"Deleting "<<pos<<" :"<<endl;
					ll.display();					
				}
				cout<<"Do you want to continue with operations(y/n):";
				cin>>choice;
			}
		}
		if(op=="char"){
			int ch,n,pos;
			char choice,el;
			LinkedList<char> ll;
			ll.createLL();
			cout<<"Created LinkedList:";
			ll.display();
			cout<<"Start operations(y/n)?:";
			cin>>choice;
			while(choice=='y'){
				cout<<"Choose the actions to perform:"<<endl;
				cout<<"1.Insert at End \n2.Insert at Beginning \n3.Inserting after a key \n4.Inserting Before a key"<<endl;
				cout<<"5.Delete at End \n6.Delete at Beginning \n7.Delete at specified position"<<endl;
				cin>>ch;
				if(ch==1){
					cout<<"How many elements do you want to insert at end?";
					cin>>n;
					for(int i=0;i<n;i++){
						cout<<"Enter the element:";
						cin>>el;			
						ll.LL_insert_at_End(el);
						cout<<"Inserting at the end:"<<endl;
						ll.display();
					}
				}
				if(ch==2){
					cout<<"How many elements do you want to insert at beginning?";
					cin>>n;
					for(int i=0;i<n;i++){
						cout<<"Enter the element:";
						cin>>el;			
						ll.LL_insert_at_Beg(el);
						cout<<"Inserting at the beginnning:"<<endl;
						ll.display();
					}
				}
				if(ch==3){					
					cout<<"Enter the element:";
					cin>>el;
					cout<<"Enter the position to insert";
					cin>>pos;			
					ll.LL_insert_at_Pos(el,pos);
					cout<<"Inserting at "<<pos<<"th position:"<<endl;
					ll.display();
				}
				if(ch==4){			
						ll.LL_Del_at_End();
						cout<<"Deleting at the end:"<<endl;
						ll.display();				
				}
				if(ch==5){			
						ll.LL_Del_at_Beg();
						cout<<"Delete at the Beginning:"<<endl;
						ll.display();
				}
				if(ch==6){
					cout<<"Enter the key to delete";
					cin>>pos;			
					ll.LL_Del_a_key(pos);
					cout<<"Deleting "<<pos<<" :"<<endl;
					ll.display();					
				}
				cout<<"Do you want to continue with operations(y/n):";
				cin>>choice;
			}
		}
		if(op=="float"){
			int ch,n,pos;
			char choice;
			float el;
			LinkedList<float> ll;
			ll.createLL();
			cout<<"Created LinkedList:";
			ll.display();
			cout<<"Start operations(y/n)?:";
			cin>>choice;
			while(choice=='y'){
				cout<<"Choose the actions to perform:"<<endl;
				cout<<"1.Insert at End \n2.Insert at Beginning \n3.Inserting after a key \n4.Inserting Before a key"<<endl;
				cout<<"5.Delete at End \n6.Delete at Beginning \n7.Delete at specified position"<<endl;
				cin>>ch;
				if(ch==1){
					cout<<"How many elements do you want to insert at end?";
					cin>>n;
					for(int i=0;i<n;i++){
						cout<<"Enter the element:";
						cin>>el;			
						ll.LL_insert_at_End(el);
						cout<<"Inserting at the end:"<<endl;
						ll.display();
					}
				}
				if(ch==2){
					cout<<"How many elements do you want to insert at beginning?";
					cin>>n;
					for(int i=0;i<n;i++){
						cout<<"Enter the element:";
						cin>>el;			
						ll.LL_insert_at_Beg(el);
						cout<<"Inserting at the beginnning:"<<endl;
						ll.display();
					}
				}
				if(ch==3){					
					cout<<"Enter the element:";
					cin>>el;
					cout<<"Enter the position to insert";
					cin>>pos;			
					ll.LL_insert_at_Pos(el,pos);
					cout<<"Inserting at "<<pos<<"th position:"<<endl;
					ll.display();
				}
				if(ch==4){			
						ll.LL_Del_at_End();
						cout<<"Deleting at the end:"<<endl;
						ll.display();				
				}
				if(ch==5){			
						ll.LL_Del_at_Beg();
						cout<<"Delete at the Beginning:"<<endl;
						ll.display();
				}
				if(ch==6){
					cout<<"Enter the key to delete";
					cin>>pos;			
					ll.LL_Del_a_key(pos);
					cout<<"Deleting "<<pos<<" :"<<endl;
					ll.display();					
				}
				cout<<"Do you want to continue with operations(y/n):";
				cin>>choice;
			}
		}
		cout<<"Do you want to continue creating(y/n):";
		cin>>stat;
	}
	

	return 0;
}
