#include"LinkedList.h"

template<class T>	
Node<T>::Node(T data){
	this->data=data;
	this->link=NULL;
}

template<class T>
T Node<T>:: getData(){
	return data;
}

template<class T>
Node<T>* Node<T>:: getLink(){
	return link;
}

template<class T>
void Node<T>::setLink(Node<T>* link){
	this->link=link;
}

template<class T>
void Node<T>::setData(T data){
	this->data=data;
}


template<class T>
LinkedList<T>::LinkedList(){
	head=NULL;
}

template<class T>
void LinkedList<T>::createLL(){
	T x;
	char option;
	Node<T>* current;
	current=head;
	do{
		cout<<"Enter the data:";
		cin>>x;
		Node<T>* node=new Node<T>(x);
		if(head==NULL){
			head=node;
			current=head;
		}
		else{
			current->setLink(node);
			current=node;
		}
		cout<<"Do you want to enter data(y/n):";
		cin>>option;
	}while(option=='y');
}

template<class T>
void LinkedList<T>::display(){
	Node<T>* p=head;
	while (p!=NULL){
		cout<<p->getData()<<" ";
		p=p->getLink();
	}
	cout<<endl;
}

template<class T>
void LinkedList<T>::LL_insert_at_Beg(T key){
	Node<T>* node1=new Node<T>(key);
	node1->setLink(head);
	head=node1;
}

template<class T>
void LinkedList<T>::LL_insert_at_End(T key){
	Node<T>* p=head;
	while (p->getLink() !=NULL){
		p=p->getLink();
	}
	Node<T>* node2=new Node<T>(key);
	p->setLink(node2);
	
}
template<class T>
void LinkedList<T>::LL_insert_at_Pos(T key,int pos){
	int count=1;
	Node<T>* p=head;
	while(p->getLink()!=NULL and count<pos-1){
		p->getLink();
		count++;
	}
	Node<T>* q=p->getLink();
	Node<T>* node=new Node<T>(key);
	node->setLink(q);
	p->setLink(node);
	
}
template<class T>
void LinkedList<T>::LL_Del_at_Beg(){
	Node<T>* temp=head;
	head=head->getLink();
	delete temp;	
}

template<class T>
void LinkedList<T>::LL_Del_at_End(){
	Node<T>* p= head;
	Node<T>* q=p->getLink();
	while(q->getLink()!=NULL){
		p=q;
		q=q->getLink();
	}
	p->setLink(NULL);
	delete q;
}

template<class T>
void LinkedList<T>::LL_Del_a_key(T key){
	Node<T>* p=head;
	Node<T>* q=p->getLink();
	while(q!=NULL && q->getData()!=key){
		p=q;
		q=q->getLink();
	}
	p->setLink(q->getLink());
	delete q;
}
