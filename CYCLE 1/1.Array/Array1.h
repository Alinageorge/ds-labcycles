#include<iostream>
using namespace std;
template <class T>
class Array{
		int LB,UB;
		T A[100];
	public:
		Array();
		Array(int,int,T[]);
		void insert_at_end(T);
		void insert_at_beg(T);
		void insert_at_index(T,int);
		void del_at_end();
		void del_at_beg();
		void del_at_index(int);
		int getIndex(T);
		void display();
};
